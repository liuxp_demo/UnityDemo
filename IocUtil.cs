﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity;

namespace UnityDemo
{
    public class IocUtil
    {
        static IUnityContainer container = null;

        public static void RegisterContainer()
        {
            container = new UnityContainer();
            //普通模式
            container.RegisterType<IUser, User1>("user1");
            //单例模式
            container.RegisterSingleton<IUser, User2>("user2");
            //带参数模式
            container.RegisterType<IUser, User1>("user3");

        }

        public static IUser GetUser(string name)
        {
            return container.Resolve<IUser>(name);
            //return new User1();
        }
    }
}
