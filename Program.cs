﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnityDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            IocUtil.RegisterContainer();
            for (int i = 0; i < 10; i++)
            {
                IUser user = IocUtil.GetUser("user1");
                user.SayHello();
                user = IocUtil.GetUser("user2");
                user.SayHello();

                Console.WriteLine();
                System.Threading.Thread.Sleep(50);
            }
            Console.ReadLine();
        }
    }
}
