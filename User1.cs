﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity;

namespace UnityDemo
{
    
    class User1 : IUser
    {
        int index = 0;

        public User1() 
        {
            
        }

        public User1(int value, string name)
        {
            Console.WriteLine(name + " " + value);
        }

        public void SayHello()
        {
            index++;
            Console.WriteLine("user1 say hello to you "+index);
        }
    }
}
