﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace UnityDemo
{
    class User2 : IUser
    {
        public User2() { }

        public User2(int value,string name)
        {
            Console.WriteLine(name+" "+value);
        }

        int index = 0;
        public void SayHello()
        {
            index++;
            Console.WriteLine("user2 say hello to you "+index);
        }
    }
}
